from globelly import *
sys.path.append('./mod_diff_data')
import analyze
from log import write_log

def main():
    write_log('diff','------> 对比报告 <------')
    for url in Queue:
        todaypack,lastdaypack = analyze.url_to_todayandlastpack(url)
        print('today: {}个, lastday: {}个'.format(len(todaypack),len(lastdaypack)))
        if todaypack or lastdaypack:
            analyze.diff_packages(todaypack, lastdaypack)