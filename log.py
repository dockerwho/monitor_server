import datetime
from globelly import Logfile

def get_date(): # 获取今天日期  eg：2022-11-11
    result=datetime.date.today()
    date = str(result)
    # print(date,type(date))
    return date


def get_time():  # 获取今天时间  eg：10:11:12.56987
    result = datetime.datetime.now()
    time = str(result.time())
    # print(time,type(time))
    return time

def write_log(mod, info):
    if type(info) == str:
        f = open(Logfile, 'a', encoding="UTF-8")
        f.write('{} {}: [{}] {}\n'.format(get_date(),get_time(),mod, info))
        f.close()
    if type(info) == list:
        f = open(Logfile, 'a', encoding="UTF-8")
        for mumber in info:
            f.write('{} {}: [{}] {}\n'.format(get_date(), get_time(), mod, mumber))
        f.close()
