# monitor_service_update
## 功能
用来监控外网源服务器是否发生了更新。

## 模块设置
1.assistant.py
程序初始化相关内容

2.globelly.py
工程中用到的所有全局变量

3.record.py
数据记录
* 日志记录模块
* 数据记录模块
* 时间获取模块

## 类的设计
1.设计一个PackagesWeb类
含有属性
* url

含有方法
* html下载
* 解析html



## 目录结构
工作目录(WorkDir)
```text
-- index 索引文件（初始化创建）
-- databases/ 存储每日信息（初始化创建）
-- 

```

databases/ 目录
```text
# WorkDir/databases/日期/大版本/小版本/库/架构
-- 2022-07-07
    -- V10
        -- V10SP1
            -- base
                -- x86_64(file)    
-- 2022-07-08
-- 2022-07-09
```

