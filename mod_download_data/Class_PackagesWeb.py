import urllib.request
from bs4 import BeautifulSoup


class PackagesWeb:
    def __init__(self, url):
        self.url = url  # 外网源含有rpm包的网站地址

    #
    #   获取网站html源码
    #   返回值 含有html源码的文本
    def __url_html(self):
        Headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36'}
        response = urllib.request.Request(url=self.url, headers=Headers)
        html = urllib.request.urlopen(response)
        # print(type(html))
        return html

    #
    #   分析html文件获取rpm列表
    #   返回值：含有网站中所有rpm名字的列表。
    def __html_analyse_rpm(self):
        # !!!此代码段仍需加强网站不通时候的处理方法。
        web_pack_list = []
        html = self.__url_html()
        soup = BeautifulSoup(html, "html.parser")
        # 分析网站内有哪些rpm安装包
        for packlist in soup.find_all('a'):
            name = packlist['href']
            if "rpm" in name:
                if "%2B" in name:
                    name = str(name).replace('%2B', '+')  # 这里是因为链接有些使用 %2B 代替了 ‘+’
                web_pack_list.append(name.lstrip().rstrip())
        return web_pack_list

    def get_rpm_list(self):
        return self.__html_analyse_rpm()