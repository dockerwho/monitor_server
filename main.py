import assistant
from globelly import *
from mod_download_data import download_main
from mod_diff_data import diff_main
from log import write_log,get_date

if __name__ == '__main__':
    assistant.project_init()    # 生成任务队列
    write_log('main',"########## {} 今日任务开始 ##########".format(get_date()))
    download_main.main()
    diff_main.main()
    write_log('main',"########## {} 今日任务完毕 ##########\n".format(get_date()))


